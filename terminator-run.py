import windows_request
import path_converter
import os

current_working_dir = os.getcwd()
current_working_dir = path_converter.from_windows_to_linux(current_working_dir)
windows_request.send_request("terminator --working-directory=%s" % current_working_dir)
