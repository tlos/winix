import os
import config

__WINDOWS_PATH_SEPARATOR__ = '\\'
__LINUX_PATH_SEPARATOR__ = "/"
__CYGDRIVE_PREFIX__ = "/cygdrive/"


def from_windows_to_linux(path):
	if path.startswith(__CYGDRIVE_PREFIX__):
		path = path.replace(__CYGDRIVE_PREFIX__, "", 1)
	else:
		path = path.replace(__WINDOWS_PATH_SEPARATOR__, __LINUX_PATH_SEPARATOR__)
		path = path[0:1].lower() + path[2:]
	return config.WINDOWS_DRIVES_PREFIX_DIR + path

def from_linux_to_windows(path):
	path = path.replace(config.WINDOWS_DRIVES_PREFIX_DIR,"",1)
	path = path[0:1] + ":" + path[1:]
	path = path.replace(__LINUX_PATH_SEPARATOR__,__WINDOWS_PATH_SEPARATOR__)
	return path


if __name__ == '__main__':
	current_working_dir = os.getcwd()
	print(from_windows_to_linux(current_working_dir))
	test_linux_path = "/c/Programy/eclipse/eclipse"
	print("Linux test %s = %s" % (test_linux_path, from_linux_to_windows(test_linux_path)))

