import socket
import sys
from config import *


def send_request(command):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((VIRTUAL_MACHINE_IP,PORT))
	s.send(command)
	result = s.recv(32768)
	print(result)
	s.close();

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print("Wrong number of arguments. You should write command")
		exit()
	command = sys.argv[1]
	send_request(command)