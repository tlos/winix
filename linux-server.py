import socket
import threading
import os
from config import *
import time
#import subprocess

def log(msg):
	print("LOG<" + str(time.ctime()) +"> ~>" + msg) # for future extension

class ServletThead(threading.Thread):
	def __init__(self, client_socket):
		threading.Thread.__init__(self)
		self.client_socket = client_socket

	def run(self):
		command = self.client_socket.recv(2048)
		log("Got command %s" % command)
		command = "export DISPLAY="+X11_IP_ADDRESS+":0.0; " + command 
		#os.system(command)
		(child_stdin, child_stdout, child_stderr) = os.popen3(command)
		result = "STD_OUT:\n" + child_stdout.read() + "\nSTD_ERR:\n" + child_stderr.read()
		log("Result->"+result)
		self.client_socket.send(result)
		self.client_socket.close()



server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(('', PORT))
server_socket.listen(5)

while True:
	client_socket,addr = server_socket.accept()
	log("Got request from %s -> processing" % str(addr))
	ServletThead(client_socket).start()
